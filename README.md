# p2pExtension #

The "Ice Cream Bar" chrome extension that provides useful shortcuts for LA Times producers.

### Set up ###

Clone the repo

`git clone git@bitbucket.org:teamnicco/p2pextension.git`

Branch it off

```
cd p2pextension
git checkout -b v1.x.x
```

Visit the Chrome extensions settings page

`chrome://extensions/`

Check the developer mode checkbox and click the "Load unpacked extension" button. Open the icecreambar folder.

### Development ###

For help with understanding the nuts and bolts of chrome extension development, read through the [documentation](https://developer.chrome.com/extensions/devguide).

When you make any changes, go back to the settings page and simply refresh the page or click "Reload" under the Ice Cream Bar. This is required for your changes to be updated because Chrome caches the extension into its memory.

### Packaging for release ###

Update icecreambar/manifest.json with the new version number.

Zip compress the icecreambar folder and rename it to include the version number like so: `icecreambar1.x.x.zip`

Upload the zip file to the [chrome web store](https://chrome.google.com/webstore/developer/edit/hlhcopmchpiajnjfojomcbhfbiebekcn) and publish your changes.