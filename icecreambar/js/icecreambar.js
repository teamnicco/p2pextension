var DEFAULT_SETTINGS = {
  "enabled": "true",
  "link_target": "_self",
  "market": "latimes",
  "whitelisted_domains": [
    "latimes.com",
    "tribuneinteractive.com",
    "newsinc.com",
    "tribdev.com",
    "desk-net.com",
    "sandiegouniontribune.com"
  ]
};

chrome.storage.local.get(DEFAULT_SETTINGS, function(settings) {
  /* Only show the bar is the domain is whitelisted */
  var show_the_bar = false;
  $.each(settings.whitelisted_domains, function( i, domain ) {
    if(window.location.hostname.includes(domain)) show_the_bar = true;
  });

  if (settings.enabled && show_the_bar) {
    var market_config_url = chrome.extension.getURL("/js/config/" + settings.market + ".json");

    var xhr = new XMLHttpRequest();
    xhr.open("GET", market_config_url, true);
    xhr.onreadystatechange = function(data) {
      if (xhr.readyState == 4) {
        render_bar(settings, JSON.parse(data.target.response));
      }
    }
    xhr.send();
  }
});

function render_bar(settings, data) {
  var link_target = settings.link_target;
  nav = data["nav"];
  bookmarks = data["bookmarks"];

  var articleID = $('.trb_allContentWrapper').data('content-id');
  articleSlug = $('.trb_allContentWrapper').data('content-slug');

  $("body").prepend('<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">');

  if (window.location.href.indexOf("stage.tribdev.com") > -1) {
    var BASE_P2P_URL = "https://content.p2p.tribstage.com";
  } else {
    var BASE_P2P_URL = "https://content.p2p.tribuneinteractive.com";
  }

  $("body").addClass("icbWrapper");
  $("body").prepend("<div class='icb'></div>");

  if (typeof articleID != 'undefined') {
    nav.push({
      "label": "Clear FB Cache",
      "icon": "facebook",
      "link": '#refreshFBcache'
    });

    nav.push({
      "label": "Edit (SNAP)",
      "icon": "hand-o-up",
      "link": data["snap_url"] + '?slug=' + articleSlug
    });

    nav.push({
      "label": "Edit (P2P)",
      "icon": "pencil",
      "link": BASE_P2P_URL + '/content_items/' + articleID + '/edit'
    });

    if(data["using_desknet"]) {
      nav.push({
        "label": "Search Desk-Net",
        "icon": "chevron-right",
        "link": 'https://www.desk-net.com/searchPage.htm?searchString=' + articleSlug
      });
    }

    nav.push({
      "label": "Slug",
      "icon": "link",
      "link": '#getslug'
    });

  }
  var p2pSlug = $('input#content_item_slug[readonly="readonly"]').val();
  if (typeof p2pSlug != 'undefined') {
    nav.push({
      "label": "Clear FB Cache",
      "icon": "facebook",
      "link": '#refreshFBcache'
    });
    nav.push({
      "label": "Edit (SNAP)",
      "icon": "hand-o-up",
      "link": data["snap_url"] + '?slug=' + p2pSlug
    });

    if(data["using_desknet"]) {
      nav.push({
        "label": "Search Desk-Net",
        "icon": "chevron-right",
        "link": 'https://www.desk-net.com/searchPage.htm?searchString=' + p2pSlug
      });
    }

    nav.push({
      "label": "Open",
      "icon": "eye",
      "link": data["site_url"] + "/" + p2pSlug + '-story.html'
    });

    nav.push({
      "label": "Slug",
      "icon": "link",
      "link": '#getslug'
    });

  }

  nav.forEach(function(entry) {
    if (entry.link.startsWith('#')) {
      var this_link_target = "_self";
    } else {
      var this_link_target = link_target;
    }

    /* Replace the variable from the config */
    entry.link = entry.link.replace("*BASE_P2P_URL*", BASE_P2P_URL);

    $link = $("<a />", {
      title: entry.label,
      href: entry.link,
      target: this_link_target
    });
    $label = $("<span />", {
      text: entry.label,
    });
    $icon = $("<i />", {
      class: "fa fa-" + entry.icon
    });
    if (entry.hide_label == true) {
      $label.css("display", "none");
    }

    $(".icb").append($("<li />", {
      html: $link.append($icon).append($label)
    }));
  });

  $(".icb").append($("<li />", {
    id: "closeTheBar",
    html: "<a href='#'><i class='fa fa-times'></i></a>"
  }));

  $("#closeTheBar").click(function(){
    $("body").removeClass("icbWrapper");
    $(".icb").remove();
  });

  $('a[href="#refreshFBcache"]').click(function() {
    var url = window.location.href.split('?')[0]
    var go = confirm("You're being redirected to Facebook's tool for refreshing the article's headline and thumbnail \
    to the most recent versions.  Click 'Fetch \
    new scrape information' when the tool loads to update the cache.");
    if (go) {
      window.open("http://developers.facebook.com/tools/debug/og/object?q=" + url + "&fbrefresh=any");
    }
  });

  $('a[href="#getslug"]').click(function() {
    if (typeof articleID == 'undefined') {
      articleSlug = p2pSlug;
    }
    prompt("Copy Slug 😉😉😉", articleSlug);
  });

  $('a[href="#bustcache"]').click(function() {
    var url = document.URL;
    url = url.substring(0, url.indexOf("?"));
    window.open(url + "?" + Date.now() + Math.round(Math.random(100) * 10), "_self");
  });

  $('a[href="#newcontent"]').click(function() {
    if (!$('#newContentDropDown').length > 0) {
      var offset = $('[href="#newcontent"]').offset().left;
      $("body").append('<ul id="newContentDropDown" style="left:' + offset + 'px">\
    <li class="ciNewDropdownOption"><a href="' + data["snap_url"] + '" target="' + link_target + '">SNAP Story</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=story" target="' + link_target + '">P2P Story</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=htmlstory" target="' + link_target + '">HTML Story</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=storylink" target="' + link_target + '">Story Link</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=blurb" target="' + link_target + '">Blurb</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=photo" target="' + link_target + '">Photograph</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=premiumvideo" target="' + link_target + '">Premium Video</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=photogallery" target="' + link_target + '">Photo Gallery</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=premiumvideoplaylist" target="' + link_target + '">Premium Video Playlist</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=storygallery" target="' + link_target + '">Story Gallery</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=column" target="' + link_target + '">Column</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=acrobat" target="' + link_target + '">Acrobat File</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=adstory" target="' + link_target + '">Ad Story</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=post" target="' + link_target + '">Blog Post</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=columnist" target="' + link_target + '">Columnist</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=pointer" target="' + link_target + '">Content Item Pointer</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=customform" target="' + link_target + '">Custom Form</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=staff" target="' + link_target + '">Editorial Staff</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=emailform" target="' + link_target + '">Email Form</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=embeddedvideo" target="' + link_target + '">Embedded Video</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=flash" target="' + link_target + '">Flash</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=graphic" target="' + link_target + '">Graphic</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=htmlpage" target="' + link_target + '">HTML Page</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=hyperlink" target="' + link_target + '">Hyperlink</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=mapmashup" target="' + link_target + '">Map</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=mp3file" target="' + link_target + '">MP3</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=gallery" target="' + link_target + '">Multimedia Gallery</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=package" target="' + link_target + '">Package</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=poll" target="' + link_target + '">Poll</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=premiumvideostream" target="' + link_target + '">Premium Video Stream</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=pullquote" target="' + link_target + '">Pull Quote</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=searchlink" target="' + link_target + '">SearchLink</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=special" target="' + link_target + '">Special Package</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=triviaquiz" target="' + link_target + '">Trivia Quiz</a></li>\
    <li class="ciNewDropdownOption"><a href="https://content.p2p.tribuneinteractive.com/content_items/new?code=ugcphotogallery" target="' + link_target + '">UGC Photo Gallery</a></li>\
    </ul>";');

    } else {
      $('#newContentDropDown').remove();
    }
  });



  $('a[href="#bookmarks"]').click(function() {
    if (!$('#bookmarksPanel').length > 0) {
      $("body").append('<ul id="bookmarksPanel"></ul>');
      bookmarks.forEach(function(entry) {
        $("#bookmarksPanel").append('<li><a href="' + entry.link + '" target="' + link_target + '"><i class="fa fa-' + entry.icon + '"></i> <span>' + entry.label + '</span></a></li>');
      });
    } else {
      $('#bookmarksPanel').remove();
    }
  })

  $(document).mouseup(function(e) {
    var container = $("#newContentDropDown");

    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.hide();
    }
  });


  $(window).on("scroll load ready", function() {
    if ($("#tophat-container").css("position") === "fixed") {
      $("#tophat-container").css("top", "30px");
    } else {
      $("#tophat-container").css("top", "initial");
    }
  });


  $("#p2p-liveblog").on("mouseover", ".card-container", function() {
    if (!$(this).hasClass("has-edit-btn")) {
      $(this).addClass("has-edit-btn");
      var liveblog_id = $("[data-liveblog-id]").first().data("liveblog-id");
      var card_id = $(this).data("card-id");

      if (window.location.href.indexOf("tribdev.com") > -1) {
        var edit_url = "http://0.0.0.0:8000/admin/liveblog/generator/?pk=" + liveblog_id + "#" + card_id;
      } else {
        var edit_url = "http://liveblog.latimes.com/admin/liveblog/generator/?pk=" + liveblog_id + "#" + card_id;
      }

      $(this).find('.card-share-buttons').append("<a target='" + link_target + "' href='" + edit_url + "'><i class=\"fa fa-pencil liveblog-edit-btn\"></i></li>");
    }
  });
}
