$(document).ready(function() {
  $('input').blur();

  var office_space_images = [
    "http://blog.memes.com/wp-content/uploads/2015/03/vacation.jpg",
    "http://s2.quickmeme.com/img/30/30acc4289731fc9d504991120ab7fd68628253e512ad8f2826b5949e83d785df.jpg",
    "http://weknowmemes.com/generator/uploads/generated/g1384740369505006881.jpg",
    "http://big.assets.huffingtonpost.com/1120officespace2.gif",
    "https://s-media-cache-ak0.pinimg.com/736x/d0/a9/4b/d0a94be140bb4b41a911ca0b93e903d5.jpg",
    "https://s-media-cache-ak0.pinimg.com/736x/2c/98/34/2c983465c17e9ea0005195e633b15046.jpg",
    "http://i.imgur.com/5zBV6he.jpg",
    "http://carboncostume.com/wordpress/wp-content/uploads/2013/03/miltonwaddams.jpg",
    "https://cdn.meme.am/instances/37242223.jpg",
    "https://cdn.meme.am/instances/500x/50922655.jpg",
    "http://big.assets.huffingtonpost.com/2014officespace2.gif",
    "https://s-media-cache-ak0.pinimg.com/236x/52/97/11/529711a9a6a901524eea2b94b27b2df0.jpg",
    "https://s-media-cache-ak0.pinimg.com/236x/46/6f/88/466f88c60bfa484448ed1def7d86c7f9.jpg",

  ];
  var image_url_i = Math.floor(Math.random() * office_space_images.length); // Random Index position in the array
  var image_url = office_space_images[image_url_i];

  $("#office-space").attr("src", image_url);

  var DEFAULT_SETTINGS = {
    "enabled": "true",
    "link_target": "_self",
    "market": "latimes",
    "whitelisted_domains": [
      "latimes.com",
      "tribuneinteractive.com",
      "newsinc.com",
      "tribdev.com",
      "desk-net.com",
      "sandiegouniontribune.com"
    ]
  };

  chrome.storage.local.get(DEFAULT_SETTINGS, function(data) {
    if (data.enabled) {
      $("#bar-enabled").attr('checked', 'checked');
    } else {
      $("#bar-enabled").removeAttr('checked');
    }

    if (data.link_target == "_blank") {
      $("#link-target").attr('checked', 'checked');
    } else {
      $("#link-target").removeAttr('checked');
    }

    $("#market").blur().val(data.market);


    $("#whitelisted-domains").val(data.whitelisted_domains);

    var save_domains = function() {
      var domains = $("#whitelisted-domains").tagit("assignedTags");
      /* sort by length ascending */
      domains.sort(function(a, b) {
        return b.length < a.length;
      });
      chrome.storage.local.set({
        "whitelisted_domains": domains
      });
    };

    $('#whitelisted-domains').tagit({
      availableTags: DEFAULT_SETTINGS["whitelisted_domains"],
      placeholderText: "Add domain",
      removeConfirmation: true,
      preprocessTag: function(input) {
        return parseUri(input).host;
      },
      afterTagAdded: save_domains,
      afterTagRemoved: save_domains,
      autocomplete: {
        delay: 100,
        minLength: 0
      }
    });

  });

  $("#bar-enabled").change(function(e) {
    chrome.storage.local.set({
      "enabled": $(this).is(":checked")
    });

    chrome.storage.local.get("enabled", function(data) {
      $("#bar-enabled").val(data.enabled);
    });
  });

  $("#link-target").change(function(e) {
    if ($(this).is(":checked")) {
      chrome.storage.local.set({
        "link_target": "_blank"
      });
    } else {
      chrome.storage.local.set({
        "link_target": "_self"
      });
    }
  });

  $("#market").change(function(e) {
    var market = $(this).val();
    chrome.storage.local.set({
      "market": market
    });
  });


  // parseUri 1.2.2
  // (c) Steven Levithan <stevenlevithan.com>
  // MIT License

  function parseUri(str) {
    var o = parseUri.options,
      m = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
      uri = {},
      i = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function($0, $1, $2) {
      if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
  };

  parseUri.options = {
    strictMode: false,
    key: ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"],
    q: {
      name: "queryKey",
      parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
      strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
      loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
  };
});
